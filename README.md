# workshop-gitlab

This is an example of a very simple Node.js project created to run on Docker with tests running on [GitLab CI](http://gitlab.com), I've created it to do a workshop about GitLab on [Campus Party Brasil 12](https://campuse.ro/events/campus-party-brasil-2019/workshop/gitlab-uma-ferramenta-para-todo-o-ciclo-devops-cpbr12/).
